#!/bin/bash

yum install -y httpd httpd-devel httpd-tools
chkconfig --add httpd
chkconfig httpd on
service httpd stop

rm -rf /var/www/html
ln -s /vagrant /var/www/html

service httpd start

# PHP
yum install -y php php-cli php-common php-devel php-mysql

# Download starter content
cd /vagrant
sudo -u vagrant wget https://bitbucket.org/webster5361/vagrant_builds/raw/1b6d0aecef440a3130eb2b3e48faf616326d415c/files/index.html
sudo -u vagrant wget https://bitbucket.org/webster5361/vagrant_builds/raw/1b6d0aecef440a3130eb2b3e48faf616326d415c/files/info.php
service httpd restart
